<?php

namespace Drupal\background_image\EventSubscriber;

use Drupal\stage_file_proxy\EventDispatcher\AlterExcludedPathsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Defines the StageFileProxySubscriber class.
 */
class StageFileProxySubscriber implements EventSubscriberInterface {

  /**
   * Adds excluded paths for Stage File Proxy.
   *
   * @param \Drupal\stage_file_proxy\EventDispatcher\AlterExcludedPathsEvent $event
   *   The event object containing excluded paths.
   */
  public function getExcludedPaths(AlterExcludedPathsEvent $event) {
    $event->addExcludedPath('/background_image/css/');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events['stage_file_proxy.alter_excluded_paths'][] = ['getExcludedPaths'];
    return $events;
  }

}
