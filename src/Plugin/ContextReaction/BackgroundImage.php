<?php

namespace Drupal\background_image\Plugin\ContextReaction;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\context\ContextInterface;
use Drupal\context\ContextReactionPluginBase;
use Drupal\context\Form\AjaxFormTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\PluginDependencyTrait;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a content reaction.
 *
 * Set the page background as a reaction to context.
 *
 * @ContextReaction(
 *   id = "background_image",
 *   label = @Translation("Background Image")
 * )
 */
class BackgroundImage extends ContextReactionPluginBase implements ContainerFactoryPluginInterface, DependentPluginInterface {

  use AjaxFormTrait;

  use PluginDependencyTrait {
    addDependency as addDependencyTrait;
  }

  /**
   * The background image to be displayed with this reaction.
   *
   * @var array
   */
  protected $background_image = NULL;

  /**
   * The Drupal UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * The Drupal context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The plugin context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected $contextHandler;

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    UuidInterface $uuid,
    ContextRepositoryInterface $contextRepository,
    ContextHandlerInterface $contextHandler,
    AccountInterface $account,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);

    $this->uuid = $uuid;
    $this->contextRepository = $contextRepository;
    $this->contextHandler = $contextHandler;
    $this->account = $account;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('uuid'),
      $container->get('context.repository'),
      $container->get('context.handler'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Executes the plugin.
   *
   * @return \Drupal\background_image\Entity\BackgroundImage
   *   Background to be displayed.
   */
  public function execute() {
    /** @var \Drupal\background_image\Entity\BackgroundImage $backgroundImage */
    return $this->background_image ?
      $this->entityTypeManager->getStorage('background_image')
        ->load($this->background_image) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'background_image' => NULL,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();

    if (isset($configuration['background_image'])) {
      $this->background_image = $configuration['background_image'];
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [
        'background_image' => $this->background_image,
      ] + parent::getConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->t('Lets you modify a background image when a context applies');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, ContextInterface $context = NULL) {
    $entities = $this->entityTypeManager->getStorage('background_image')
      ->loadMultiple();

    $options = [];
    foreach ($entities as $entity) {
      $options[$entity->id()] = $entity->label();
    }

    $form['background_image'] = [
      '#type' => 'select',
      '#title' => $this->t('Background image'),
      '#default_value' => $this->configuration['background_image'],
      '#options' => $options,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $config['background_image'] = $form_state->getValue(['background_image'], NULL);
    $this->setConfiguration($config);
  }

}
